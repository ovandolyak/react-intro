import './Nature.scss'
import image from './image.jpg';
import React from 'react'

const Nature = () => {
  return (
    <img src={image} alt="image1" />
  )
}

export default Nature;

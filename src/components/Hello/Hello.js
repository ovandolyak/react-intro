import React from 'react'
import './Hello.scss'

const Hello = (props) => {

  return (
    <div className='hello'>{props.text}</div>
  )
}
export default Hello;
